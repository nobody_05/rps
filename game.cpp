#include<bits/stdc++.h>
#include<windows.h>

using namespace std;


void welcomeAnimation()
{
    system("cls");
    cout<<"\t\t\t  ====================================\n";
    Sleep(150);

    cout<<"\n\t\t\t\t\tW";Sleep(150);
    cout<<"E";Sleep(150);
    cout<<"L";Sleep(150);
    cout<<"C";Sleep(150);
    cout<<"O";Sleep(150);
    cout<<"M";Sleep(150);
    cout<<"E";Sleep(150);
    cout<<" T";Sleep(150);
    cout<<"O\n";Sleep(150);

    cout<<"\t\t\t\t\tT";Sleep(150);
    cout<<"H";Sleep(150);
    cout<<"E";Sleep(150);
    cout<<"  G";Sleep(150);
    cout<<"A";Sleep(150);
    cout<<"M";Sleep(150);
    cout<<"E\n";Sleep(150);

    cout<<"\t\t\t\t  R";Sleep(150);
    cout<<"O";Sleep(150);
    cout<<"C";Sleep(150);
    cout<<"K,";Sleep(150);
    cout<<" P";Sleep(150);
    cout<<"A";Sleep(150);
    cout<<"P";Sleep(150);
    cout<<"E";Sleep(150);
    cout<<"R";Sleep(150);
    cout<<" & ";Sleep(150);
    cout<<"S";Sleep(150);
    cout<<"C";Sleep(150);
    cout<<"I";Sleep(150);
    cout<<"S";Sleep(150);
    cout<<"S";Sleep(150);
    cout<<"O";Sleep(150);
    cout<<"R";Sleep(150);

    cout<<"\n\t\t\t <------------------------------------>\n\n";
    Sleep(150);
    system("cls");
}

int computer_choice()
{
    srand(time(0));
    return rand()%3;
}


int main()
{
    system("color 6");
    //welcomeRPS();

    int user_score=0;
    int pc_score=0;

    cout<<"Enter total rounds of game: ";
    int r; cin>>r;

    if(r<=0)
    {
        cout<<"Invalid input!!!"<<endl;
        cout<<"Enter total rounds of game: ";
        cin>>r;
    }
    //if(r==0)
    //{
    //    cout<<"Invalid input!!!"<<endl;
    //    cout<<"Enter total rounds of game: ";
    //    cin>>r;
    //}

    while(r--)
    {
        //system("cls");
        cout<<"User Score: "<<user_score<<endl;
        cout<<"Computer Score: "<<pc_score<<endl;

        int pc_choice=computer_choice();
        cout<<"\n0. Rock\n";
        cout<<"1. Paper\n";
        cout<<"2. Scissor\n";

        cout<<"\nUser choice : ";
        int n; cin>>n;

        cout<<"Computers Choice: "<<pc_choice<<endl;

        if(pc_choice==0 && n==1){
            user_score++;
        }
        else if(pc_choice==0 && n==2){
            pc_score++;
        }
        else if(pc_choice==1 && n==0) {
            pc_score++;
        }
        else if(pc_choice==1 && n==2) {
            user_score++;

        }
        else if(pc_choice==2 && n==0){
            user_score++;
        }
        else if(pc_choice==2 && n==1){
            pc_score++;
        }
        else if(pc_choice==n){
            cout<<"Computers Choice & User Choice are same.\nSo no one scored!\n"<<endl;
        }
        if(n>=3){
            cout<<"Invalid input!!\n"<<endl;
            r++;
            continue;
        }
        if(n<0){
            cout<<"Invalid input!!\n"<<endl;
            r++;
            continue;
        }

    }

    cout<<"Final score of User "<<user_score<<endl;
    cout<<"Final score of Computer "<<pc_score<<endl;


    if(user_score > pc_score) cout<<"User Wins the game"<<endl;
    else if(user_score < pc_score) cout<<"Computer wins the game"<<endl;
    else cout<<"Scores are equal.\nSo it's a tie!!"<<endl<<endl;

}
